#!/bin/bash -v

MONGOPATH="/usr/local/bin"
TPPATH=$HOME"/NoSQL/TP/MongoDB"

results="exo3.results"
mongo="$MONGOPATH/mongo --quiet --eval "
sep="\n******************************************************************"


printnl ()
{
echo -e "" >> $results
}

printqs ()
{
echo -e $sep >> $results
echo -e $1 >> $results
printnl
}

print ()
{
echo -e $1 >> $results
}

mong ()
{
print "> $1 ;"
$mongo "$1" >> $results
}

# remove results file
if [ -f $results ]; then
    echo "delete result file";
    rm $results;
    touch $results;
fi

# drop existing table
if [ -z "$1" ] # null string
then
    echo "drop existing DB"
    $mongo '
    use tourPedia
    '
    $mongo '
    db.paris.drop()
    '
    echo "import file tourPedia_paris.json"
    # import fichier dans table
    #./mongoimport --db tourPedia --collection paris --file $TPPATH/tourPedia_paris.json;
    mongoimport --db test --collection paris --file $TPPATH/tourPedia_paris.json;
else
    #$mongo 'use tourPedia'
    echo "no import"
fi

printqs "1. Importez le jeu de données tour-pedia.json dans une base de données “tourPedia” avec une collection “paris”"

printqs "2. Filtrez les lieux par type “accommodation” et service “blanchisserie”"
mong 'db.paris.find({$and: [{"category": "accommodation"}, {"services":"blanchisserie"}]}).count()'

printqs "3. Projetez les adresses des lieux de type “accommodation”"
mong 'db.paris.find({"category": "accommodation"}, { "location.address": 1, _id:false })'

printqs "4. Filtrez les listes de commentaires (reviews) des lieux, pour lesquelles au moins un commentaire (reviews) est écrit en anglais (en) et a une note (rating) supérieure à 3 (attention, LE commentaire en anglais doit avoir un rang de 3 ou plus)"
mong 'db.paris.find({reviews: {$elemMatch:{"language":"en", "rating":{$gte:3}}}}).count()'

printqs "5. Groupez les lieux par catégorie et comptez les"
mong 'db.paris.aggregate(
   [
      {
        $group : {
           _id : { category: "$category" },
           count: { $sum: 1 }
        }
      }
   ]
)'


printqs "6. Créez un pipeline d’agrégation pour les lieux de catégorie “accommodation”, et donnez le nombre de lieux par valeur de “services”."
mong 'db.paris.aggregate([{"$match":{"category":"accommodation"}},{"$unwind":"$services"},{"$group":{"_id":"$services","effectif":{"$sum":1}}}])'


exit 0
