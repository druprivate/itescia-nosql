#!/bin/bash

# MONGOPATH="/home/drussier/mongodb-linux-x86_64-ubuntu1604-3.6.1/bin"
MONGOPATH="/usr/local/bin"
TPPATH=$HOME"/NoSQL/TP/MongoDB"

results="exo1.results.txt"
mongo="$MONGOPATH/mongo --quiet --eval "
sep="\n******************************************************************"


printnl ()
{
echo -e "" >> $results
}

printqs ()
{
echo -e $sep >> $results
echo -e $1 >> $results
printnl
}

print ()
{
echo -e $1 >> $results
}

mong ()
{
print "> $1 ;"
$mongo "$1" >> $results
}

# remove results file
if [ -f $results ]; then
    echo "delete result file";
    rm $results;
    touch $results;
fi

# drop existing table
if [ -z "$1" ]
then
    echo "drop existing DB"
    $mongo '
    db.emails.drop()
    '
    echo "import file enron.json"
    # import fichier dans table
    mongoimport --db test --collection emails --file $TPPATH/enron.json;
else
    echo "no import"
fi

printqs "I.1) Import the enron.json in the collection \"emails\" [Shell command]"


printqs "I.2) What is the total amount of emails ? [Query + Result]"
mong 'db.emails.count()'


printqs  "I.3) What is the amount of emails sent by addresses in domain enron.com ? [Query + Result]"
mong 'db.emails.find({"sender":{$regex:"@enron.com$"}}).count()'


print "# afficher seulement sender"
mong 'db.emails.find({"sender":{$regex:"@enron.com$"}}, { sender: 1, _id:false }).limit(10)'


printqs "I.4) List the forwarded emails (subject starting with “FW:”) [Query]"
mong 'db.emails.find({"subject":{$regex:"^FW:"}}, { subject: 1, _id:false })'
mong 'db.emails.find({"subject":{$regex:"^FW:"}}).count()'


printqs "I.5) How long took the last request [Request + Time]"
mong 'db.emails.find({"subject":{$regex:"^FW:"}}, { subject: 1, _id:false }).explain("executionStats") '


printqs "I.6) Add an index the right field to make the last request run faster [Index Query]"
mong 'db.emails.createIndex( { subject: 1 } )'
mong 'db.emails.find({"subject":{$regex:"^FW:"}}, { subject: 1, _id:false }).explain("executionStats")'


printqs "I.7) How long took the last request with the index [Time]"


printqs "I.8) Find only dates, sender and subject of all messages sent to rosalee.fleming@enron.com [Query]"
mong 'db.emails.find({"to": "rosalee.fleming@enron.com"}, { subject: 1, date: 1, sender: 1, _id:false })'


printqs "I.9) Remove lizard_ar@yahoo.com from all the email recipient [Query]"
mong 'db.emails.find({"recipients": "lizard_ar@yahoo.com"}).count()'
# mong 'db.emails.remove({ "recipients": "lizard_ar@yahoo.com" },{ $pull: { "recipients": "lizard_ar@yahoo.com" } })'
mong 'db.emails.updateMany({ "recipients": "lizard_ar@yahoo.com" },{ $pull: { "recipients": "lizard_ar@yahoo.com" } })'
mong 'db.emails.find({"recipients": "lizard_ar@yahoo.com"}).count()'


printqs "I.10) Add rob.bradley@enron.com as recipient to all emails sent by rosalee.fleming@enron.com [Query]"
mong 'db.emails.find({ "sender": "rosalee.fleming@enron.com"}).count()'
mong 'db.emails.find({ "sender": "rosalee.fleming@enron.com", "recipients": "rob.bradley@enron.com" }).count()'


mong '
db.emails.updateMany({ "sender": "rosalee.fleming@enron.com" },
{ $push: { "recipients": "rob.bradley@enron.com" } })
'

mong 'db.emails.find({ "sender": "rosalee.fleming@enron.com"}).count()'
mong 'db.emails.find({ "sender": "rosalee.fleming@enron.com", "recipients": "rob.bradley@enron.com" }).count()'


exit 0
