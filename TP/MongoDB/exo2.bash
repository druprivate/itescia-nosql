#!/bin/bash

MONGOPATH="/usr/local/bin"
TPPATH=$HOME"/NoSQL/TP/MongoDB"

results="exo2.results.txt"
mongo="$MONGOPATH/mongo --quiet --eval "
sep="\n******************************************************************"


printnl ()
{
echo -e "" >> $results
}

printqs ()
{
echo -e $sep >> $results
echo -e $1 >> $results
printnl
}

print ()
{
echo -e $1 >> $results
}

mong ()
{
print "> $1 ;"
$mongo "$1" >> $results
}

# remove results file
if [ -f $results ]; then
    echo "delete result file";
    rm $results;
    touch $results;
fi

# drop existing table
if [ -z "$1" ]
then
    echo "drop existing DB"
    $mongo '
    db.zips.drop()
    '
    echo "import file zips.json"
    # import fichier dans table
    mongoimport --db test --collection zips --file $TPPATH/zips.json;
else
    echo "no import"
fi


printqs "Import the zips.json in the collection "zips" [Shell command]"


printqs "II.1. List the 10 most populated zones in California and Louisiana [Query]"
mong 'db.zips.find({"state": "CA"}, { city: 1, state: 1, pop: -1, _id:false }).sort({"pop" : -1}).limit(10)'
mong 'db.zips.find({"state": "LA"}, { city: 1, state: 1, pop: -1, _id:false }).sort({"pop" : -1}).limit(10)'


printqs "II.2. Then most populated zones in California and Louisiana ranked 10 to 20 [Query]"
mong 'db.zips.find({"state": "CA"}, { city: 1, state: 1, pop: -1, _id:false }).sort({"pop" : -1}).limit(10).skip(10)'
mong 'db.zips.find({"state": "LA"}, { city: 1, state: 1, pop: -1, _id:false }).sort({"pop" : -1}).limit(10).skip(10)'


printqs "II.3. Add a field country with the value USA to all the zips [Query]"
mong 'db.zips.findOne()'
#mong 'db.zips.update({}, {$set: {"country": "USA"}}, {upsert:false, multi: true})'
mong 'db.zips.updateMany({}, {$set: {"country": "USA"}})'
mong 'db.zips.findOne()'


printqs "II.4. List all zones with more than 100 000 inhabitant located on the west side of meridian -74. [Query]"
mong 'db.zips.find({"pop": {$gte : 100000}, "loc.0": {$lte : -74}}).limit(10)'


printqs "II.5. What is the closest zone to coordinates -73.996705, 40.74838 [Query + Answer]"
mong 'db.zips.createIndex( { loc : "2d" } )'
mong '
db.zips.find( { 'loc': {$near : [ -73.996705, 40.74838 ] } }, { state: 1, city: 1, loc: 1, _id:true } ).limit(1)
'

printqs "Quelle commande retourne le code postal le plus proche des coordonnées -73.996705, 40.74838"
mong '
db.zips.find( { 'loc': {$near : [ -73.996705, 40.74838 ] } }, { _id:true } ).limit(1)
'

# https://stackoverflow.com/questions/7837731/units-to-use-for-maxdistance-and-mongodb
# 1 degree == 111.12 km
printqs "II.6. The cities that are less than 5km away from -73.996705, 40.74838: [Query + Answer]"
mong '
db.zips.find( { 'loc': {$near : [ -73.996705, 40.74838 ] , $maxDistance: 5/111.12} }, { state: 1, city: 1, loc: 1, _id:false } )
'

printqs "Other solution"
# https://docs.mongodb.com/manual/geospatial-queries/#example
mong 'db.zips.dropIndexes()'
mong 'db.zips.createIndex( { loc : "2dsphere" } )'
mong '
db.zips.find(
   {
     loc:
       { $near:
          {
            $geometry: { type: "Point",  coordinates: [ -73.996705, 40.74838 ] },
            $maxDistance: 5000
          }
       }
   },
  { state: 1, city: 1, loc: 1, _id:false }
)
'

printqs "II.7. The cities that have more than 500 000 inhabitants. [Query + Answer]"
mong '
db.zips.aggregate(
   [
      {
        $group : {
           _id : { country: "country", state: "$state", city: "$city" },
           totalPop: { $sum: "$pop" }
        }
      },
      { $sort: { totalPop: -1 } },
      { $limit: 20 },
      { $out: "cities" }
   ]
)
'
mong 'db.cities.find({"totalPop": {$gte : 500000}})'


printqs "6. Elle ne retourne rien, quel est le problème ? Pourquoi MongoDB ne retourne pas d'erreurs lors de son exécution ?"

mong '
db.zips.find({city:{regex:"^N"}})
'

mong '
db.zips.find({$and:[{city:{regex:"^N"}},{$or:[{state:"NY"},{state:"NJ"}]}]})
'

mong '
db.zips.find({$and:[{$and:[{city:{regex:"^N"}},{$or:[{state:"NY"},{state:"NJ"}]}]},{pop:{$gt:100000,$lt:1500000}}]})
'


mong '
db.zips.find({city:{$regex:"^N"}})
'

mong '
db.zips.find({$and:[{city:{$regex:"^N"}},{$or:[{state:"NY"},{state:"NJ"}]}]})
'

mong '
db.zips.find({$and:[{$and:[{city:{$regex:"^N"}},{$or:[{state:"NY"},{state:"NJ"}]}]},{pop:{$gt:100000,$lt:1500000}}]})
'



exit 0
