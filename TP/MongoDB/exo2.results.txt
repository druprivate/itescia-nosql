
******************************************************************
Import the zips.json in the collection zips [Shell command]


******************************************************************
II.1. List the 10 most populated zones in California and Louisiana [Query]

> db.zips.find({"state": "CA"}, { city: 1, state: 1, pop: -1, _id:false }).sort({"pop" : -1}).limit(10) ;
{ "city" : "BELL GARDENS", "pop" : 99568, "state" : "CA" }
{ "city" : "LOS ANGELES", "pop" : 96074, "state" : "CA" }
{ "city" : "NORWALK", "pop" : 94188, "state" : "CA" }
{ "city" : "ARLETA", "pop" : 88114, "state" : "CA" }
{ "city" : "SOUTH GATE", "pop" : 87026, "state" : "CA" }
{ "city" : "LOS ANGELES", "pop" : 83958, "state" : "CA" }
{ "city" : "FONTANA", "pop" : 81255, "state" : "CA" }
{ "city" : "HOLLY PARK", "pop" : 78511, "state" : "CA" }
{ "city" : "WESTMINSTER", "pop" : 77965, "state" : "CA" }
{ "city" : "SANTA ANA", "pop" : 77151, "state" : "CA" }
> db.zips.find({"state": "LA"}, { city: 1, state: 1, pop: -1, _id:false }).sort({"pop" : -1}).limit(10) ;
{ "city" : "MARRERO", "pop" : 58905, "state" : "LA" }
{ "city" : "NEW ORLEANS", "pop" : 56494, "state" : "LA" }
{ "city" : "NEW IBERIA", "pop" : 56105, "state" : "LA" }
{ "city" : "KENNER", "pop" : 54023, "state" : "LA" }
{ "city" : "LAKE CHARLES", "pop" : 49710, "state" : "LA" }
{ "city" : "NEW ORLEANS", "pop" : 47894, "state" : "LA" }
{ "city" : "NEW ORLEANS", "pop" : 47077, "state" : "LA" }
{ "city" : "OPELOUSAS", "pop" : 46673, "state" : "LA" }
{ "city" : "METAIRIE", "pop" : 46193, "state" : "LA" }
{ "city" : "NEW ORLEANS", "pop" : 45119, "state" : "LA" }

******************************************************************
II.2. Then most populated zones in California and Louisiana ranked 10 to 20 [Query]

> db.zips.find({"state": "CA"}, { city: 1, state: 1, pop: -1, _id:false }).sort({"pop" : -1}).limit(10).skip(10) ;
{ "city" : "INDUSTRY", "pop" : 77114, "state" : "CA" }
{ "city" : "COAST GUARD ISLA", "pop" : 76110, "state" : "CA" }
{ "city" : "RIALTO", "pop" : 75341, "state" : "CA" }
{ "city" : "LOS ANGELES", "pop" : 74751, "state" : "CA" }
{ "city" : "LONG BEACH", "pop" : 74011, "state" : "CA" }
{ "city" : "HUNTINGTON PARK", "pop" : 72139, "state" : "CA" }
{ "city" : "MORENO VALLEY", "pop" : 71314, "state" : "CA" }
{ "city" : "LAKE LOS ANGELES", "pop" : 71024, "state" : "CA" }
{ "city" : "SAN FRANCISCO", "pop" : 70770, "state" : "CA" }
{ "city" : "IRWINDALE", "pop" : 69464, "state" : "CA" }
> db.zips.find({"state": "LA"}, { city: 1, state: 1, pop: -1, _id:false }).sort({"pop" : -1}).limit(10).skip(10) ;
{ "city" : "NEW ORLEANS", "pop" : 45070, "state" : "LA" }
{ "city" : "LAKE CHARLES", "pop" : 42627, "state" : "LA" }
{ "city" : "NEW ORLEANS", "pop" : 40049, "state" : "LA" }
{ "city" : "METAIRIE", "pop" : 39554, "state" : "LA" }
{ "city" : "TERRYTOWN", "pop" : 37901, "state" : "LA" }
{ "city" : "THIBODAUX", "pop" : 37831, "state" : "LA" }
{ "city" : "KOLIN", "pop" : 37069, "state" : "LA" }
{ "city" : "HARVEY", "pop" : 36824, "state" : "LA" }
{ "city" : "MONROE", "pop" : 35643, "state" : "LA" }
{ "city" : "BRIDGE CITY", "pop" : 35200, "state" : "LA" }

******************************************************************
II.3. Add a field country with the value USA to all the zips [Query]

> db.zips.findOne() ;
{
	"_id" : "01007",
	"city" : "BELCHERTOWN",
	"loc" : [
		-72.410953,
		42.275103
	],
	"pop" : 10579,
	"state" : "MA"
}
> db.zips.updateMany({}, {$set: {"country": "USA"}}) ;
{ "acknowledged" : true, "matchedCount" : 29353, "modifiedCount" : 29353 }
> db.zips.findOne() ;
{
	"_id" : "01007",
	"city" : "BELCHERTOWN",
	"loc" : [
		-72.410953,
		42.275103
	],
	"pop" : 10579,
	"state" : "MA",
	"country" : "USA"
}

******************************************************************
II.4. List all zones with more than 100 000 inhabitant located on the west side of meridian -74. [Query]

> db.zips.find({"pop": {$gte : 100000}, "loc.0": {$lte : -74}}).limit(10) ;
{ "_id" : "60623", "city" : "CHICAGO", "loc" : [ -87.7157, 41.849015 ], "pop" : 112047, "state" : "IL", "country" : "USA" }

******************************************************************
II.5. What is the closest zone to coordinates -73.996705, 40.74838 [Query + Answer]

> db.zips.createIndex( { loc : "2d" } ) ;
{
	"createdCollectionAutomatically" : false,
	"numIndexesBefore" : 1,
	"numIndexesAfter" : 2,
	"ok" : 1
}
> db.zips.find( { loc: {$near : [ -73.996705, 40.74838 ] } }, { state: 1, city: 1, loc: 1, _id:true } ).limit(1) ;
{ "_id" : "10001", "city" : "NEW YORK", "loc" : [ -73.996705, 40.74838 ], "state" : "NY" }

******************************************************************
Quelle commande retourne le code postal le plus proche des coordonnées -73.996705, 40.74838

> db.zips.find( { loc: {$near : [ -73.996705, 40.74838 ] } }, { _id:true } ).limit(1) ;
{ "_id" : "10001" }

******************************************************************
II.6. The cities that are less than 5km away from -73.996705, 40.74838: [Query + Answer]

> db.zips.find( { loc: {$near : [ -73.996705, 40.74838 ] , $maxDistance: 5/111.12} }, { state: 1, city: 1, loc: 1, _id:false } ) ;
{ "city" : "NEW YORK", "loc" : [ -73.996705, 40.74838 ], "state" : "NY" }
{ "city" : "NEW YORK", "loc" : [ -73.992503, 40.754713 ], "state" : "NY" }
{ "city" : "NEW YORK", "loc" : [ -73.99963, 40.740225 ], "state" : "NY" }
{ "city" : "NEW YORK", "loc" : [ -73.991826, 40.759724 ], "state" : "NY" }
{ "city" : "NEW YORK", "loc" : [ -74.005421, 40.73393 ], "state" : "NY" }
{ "city" : "NEW YORK", "loc" : [ -73.982347, 40.759729 ], "state" : "NY" }
{ "city" : "NEW YORK", "loc" : [ -73.989223, 40.731253 ], "state" : "NY" }
{ "city" : "NEW YORK", "loc" : [ -73.981328, 40.737476 ], "state" : "NY" }
{ "city" : "NEW YORK", "loc" : [ -73.978134, 40.744281 ], "state" : "NY" }
{ "city" : "NEW YORK", "loc" : [ -73.985834, 40.765069 ], "state" : "NY" }
{ "city" : "NEW YORK", "loc" : [ -73.998284, 40.72553 ], "state" : "NY" }
{ "city" : "NEW YORK", "loc" : [ -73.970661, 40.75172 ], "state" : "NY" }
{ "city" : "NEW YORK", "loc" : [ -73.979591, 40.726188 ], "state" : "NY" }
{ "city" : "NEW YORK", "loc" : [ -74.002529, 40.718511 ], "state" : "NY" }
{ "city" : "NEW YORK", "loc" : [ -73.982652, 40.77638 ], "state" : "NY" }
{ "city" : "NEW YORK", "loc" : [ -73.965703, 40.757091 ], "state" : "NY" }
{ "city" : "NEW YORK", "loc" : [ -73.987681, 40.715231 ], "state" : "NY" }
{ "city" : "NEW YORK", "loc" : [ -74.007022, 40.713905 ], "state" : "NY" }
{ "city" : "HOBOKEN", "loc" : [ -74.032863, 40.7445 ], "state" : "NJ" }
{ "city" : "NEW YORK", "loc" : [ -74.001298, 40.710092 ], "state" : "NY" }
Type "it" for more

******************************************************************
Other solution

> db.zips.dropIndexes() ;
{
	"nIndexesWas" : 2,
	"msg" : "non-_id indexes dropped for collection",
	"ok" : 1
}
> db.zips.createIndex( { loc : "2dsphere" } ) ;
{
	"createdCollectionAutomatically" : false,
	"numIndexesBefore" : 1,
	"numIndexesAfter" : 2,
	"ok" : 1
}
> db.zips.find( { loc: { $near: { $geometry: { type: "Point", coordinates: [ -73.996705, 40.74838 ] }, $maxDistance: 5000 } } }, { state: 1, city: 1, loc: 1, _id:false } ) ;
{ "city" : "NEW YORK", "loc" : [ -73.996705, 40.74838 ], "state" : "NY" }
{ "city" : "NEW YORK", "loc" : [ -73.992503, 40.754713 ], "state" : "NY" }
{ "city" : "NEW YORK", "loc" : [ -73.99963, 40.740225 ], "state" : "NY" }
{ "city" : "NEW YORK", "loc" : [ -73.991826, 40.759724 ], "state" : "NY" }
{ "city" : "NEW YORK", "loc" : [ -73.978134, 40.744281 ], "state" : "NY" }
{ "city" : "NEW YORK", "loc" : [ -73.982347, 40.759729 ], "state" : "NY" }
{ "city" : "NEW YORK", "loc" : [ -74.005421, 40.73393 ], "state" : "NY" }
{ "city" : "NEW YORK", "loc" : [ -73.981328, 40.737476 ], "state" : "NY" }
{ "city" : "NEW YORK", "loc" : [ -73.989223, 40.731253 ], "state" : "NY" }
{ "city" : "NEW YORK", "loc" : [ -73.985834, 40.765069 ], "state" : "NY" }
{ "city" : "NEW YORK", "loc" : [ -73.970661, 40.75172 ], "state" : "NY" }
{ "city" : "NEW YORK", "loc" : [ -73.998284, 40.72553 ], "state" : "NY" }
{ "city" : "NEW YORK", "loc" : [ -73.965703, 40.757091 ], "state" : "NY" }
{ "city" : "NEW YORK", "loc" : [ -73.979591, 40.726188 ], "state" : "NY" }
{ "city" : "HOBOKEN", "loc" : [ -74.032863, 40.7445 ], "state" : "NJ" }
{ "city" : "NEW YORK", "loc" : [ -73.982652, 40.77638 ], "state" : "NY" }
{ "city" : "NEW YORK", "loc" : [ -74.002529, 40.718511 ], "state" : "NY" }
{ "city" : "WEEHAWKEN", "loc" : [ -74.030558, 40.768153 ], "state" : "NJ" }
{ "city" : "NEW YORK", "loc" : [ -73.987681, 40.715231 ], "state" : "NY" }
{ "city" : "NEW YORK", "loc" : [ -73.958805, 40.768476 ], "state" : "NY" }
Type "it" for more

******************************************************************
II.7. The cities that have more than 500 000 inhabitants. [Query + Answer]

> db.zips.aggregate( [ { $group : { _id : { country: "country", state: "$state", city: "$city" }, totalPop: { $sum: "$pop" } } }, { $sort: { totalPop: -1 } }, { $limit: 20 }, { $out: "cities" } ] ) ;
> db.cities.find({"totalPop": {$gte : 500000}}) ;
{ "_id" : { "country" : "country", "state" : "IL", "city" : "CHICAGO" }, "totalPop" : 2452177 }
{ "_id" : { "country" : "country", "state" : "NY", "city" : "BROOKLYN" }, "totalPop" : 2300504 }
{ "_id" : { "country" : "country", "state" : "CA", "city" : "LOS ANGELES" }, "totalPop" : 2102295 }
{ "_id" : { "country" : "country", "state" : "TX", "city" : "HOUSTON" }, "totalPop" : 2095918 }
{ "_id" : { "country" : "country", "state" : "PA", "city" : "PHILADELPHIA" }, "totalPop" : 1610956 }
{ "_id" : { "country" : "country", "state" : "NY", "city" : "NEW YORK" }, "totalPop" : 1476790 }
{ "_id" : { "country" : "country", "state" : "NY", "city" : "BRONX" }, "totalPop" : 1209548 }
{ "_id" : { "country" : "country", "state" : "CA", "city" : "SAN DIEGO" }, "totalPop" : 1049298 }
{ "_id" : { "country" : "country", "state" : "MI", "city" : "DETROIT" }, "totalPop" : 963243 }
{ "_id" : { "country" : "country", "state" : "TX", "city" : "DALLAS" }, "totalPop" : 940191 }
{ "_id" : { "country" : "country", "state" : "AZ", "city" : "PHOENIX" }, "totalPop" : 890853 }
{ "_id" : { "country" : "country", "state" : "FL", "city" : "MIAMI" }, "totalPop" : 825232 }
{ "_id" : { "country" : "country", "state" : "CA", "city" : "SAN JOSE" }, "totalPop" : 816653 }
{ "_id" : { "country" : "country", "state" : "TX", "city" : "SAN ANTONIO" }, "totalPop" : 811792 }
{ "_id" : { "country" : "country", "state" : "MD", "city" : "BALTIMORE" }, "totalPop" : 733081 }
{ "_id" : { "country" : "country", "state" : "CA", "city" : "SAN FRANCISCO" }, "totalPop" : 723993 }
{ "_id" : { "country" : "country", "state" : "TN", "city" : "MEMPHIS" }, "totalPop" : 632837 }
{ "_id" : { "country" : "country", "state" : "CA", "city" : "SACRAMENTO" }, "totalPop" : 628279 }
{ "_id" : { "country" : "country", "state" : "FL", "city" : "JACKSONVILLE" }, "totalPop" : 610160 }
{ "_id" : { "country" : "country", "state" : "GA", "city" : "ATLANTA" }, "totalPop" : 609591 }

******************************************************************
6. Elle ne retourne rien, quel est le problème ? Pourquoi MongoDB ne retourne pas d'erreurs lors de son exécution ?

> db.zips.find({city:{regex:"^N"}}) ;
> db.zips.find({$and:[{city:{regex:"^N"}},{$or:[{state:"NY"},{state:"NJ"}]}]}) ;
> db.zips.find({$and:[{$and:[{city:{regex:"^N"}},{$or:[{state:"NY"},{state:"NJ"}]}]},{pop:{$gt:100000,$lt:1500000}}]}) ;
> db.zips.find({city:{$regex:"^N"}}) ;
{ "_id" : "01331", "city" : "NEW SALEM", "loc" : [ -72.214644, 42.592065 ], "pop" : 14077, "state" : "MA", "country" : "USA" }
{ "_id" : "01364", "city" : "NEW SALEM", "loc" : [ -72.305867, 42.591231 ], "pop" : 8544, "state" : "MA", "country" : "USA" }
{ "_id" : "01355", "city" : "NEW SALEM", "loc" : [ -72.306241, 42.514643 ], "pop" : 456, "state" : "MA", "country" : "USA" }
{ "_id" : "01360", "city" : "NORTHFIELD", "loc" : [ -72.450995, 42.688705 ], "pop" : 2829, "state" : "MA", "country" : "USA" }
{ "_id" : "01532", "city" : "NORTHBOROUGH", "loc" : [ -71.646372, 42.318242 ], "pop" : 11930, "state" : "MA", "country" : "USA" }
{ "_id" : "01531", "city" : "NEW BRAINTREE", "loc" : [ -72.130642, 42.31977 ], "pop" : 881, "state" : "MA", "country" : "USA" }
{ "_id" : "01535", "city" : "NORTH BROOKFIELD", "loc" : [ -72.082129, 42.266455 ], "pop" : 4755, "state" : "MA", "country" : "USA" }
{ "_id" : "01534", "city" : "NORTHBRIDGE", "loc" : [ -71.656366, 42.1494 ], "pop" : 4564, "state" : "MA", "country" : "USA" }
{ "_id" : "01537", "city" : "NORTH OXFORD", "loc" : [ -71.885953, 42.16549 ], "pop" : 3031, "state" : "MA", "country" : "USA" }
{ "_id" : "01536", "city" : "NORTH GRAFTON", "loc" : [ -71.703691, 42.229726 ], "pop" : 5401, "state" : "MA", "country" : "USA" }
{ "_id" : "01760", "city" : "NATICK", "loc" : [ -71.35741, 42.287476 ], "pop" : 30432, "state" : "MA", "country" : "USA" }
{ "_id" : "01845", "city" : "NORTH ANDOVER", "loc" : [ -71.109004, 42.682583 ], "pop" : 22792, "state" : "MA", "country" : "USA" }
{ "_id" : "01862", "city" : "NORTH BILLERICA", "loc" : [ -71.290217, 42.575694 ], "pop" : 8720, "state" : "MA", "country" : "USA" }
{ "_id" : "01863", "city" : "NORTH CHELMSFORD", "loc" : [ -71.390834, 42.634737 ], "pop" : 7878, "state" : "MA", "country" : "USA" }
{ "_id" : "01864", "city" : "NORTH READING", "loc" : [ -71.094711, 42.581898 ], "pop" : 12002, "state" : "MA", "country" : "USA" }
{ "_id" : "01908", "city" : "NAHANT", "loc" : [ -70.927739, 42.426098 ], "pop" : 3828, "state" : "MA", "country" : "USA" }
{ "_id" : "01950", "city" : "NEWBURYPORT", "loc" : [ -70.884668, 42.812964 ], "pop" : 16317, "state" : "MA", "country" : "USA" }
{ "_id" : "01951", "city" : "NEWBURY", "loc" : [ -70.847377, 42.783475 ], "pop" : 3710, "state" : "MA", "country" : "USA" }
{ "_id" : "02061", "city" : "NORWELL", "loc" : [ -70.82172, 42.159574 ], "pop" : 9279, "state" : "MA", "country" : "USA" }
{ "_id" : "02056", "city" : "NORFOLK", "loc" : [ -71.326934, 42.117746 ], "pop" : 9259, "state" : "MA", "country" : "USA" }
Type "it" for more
> db.zips.find({$and:[{city:{$regex:"^N"}},{$or:[{state:"NY"},{state:"NJ"}]}]}) ;
{ "_id" : "07031", "city" : "NORTH ARLINGTON", "loc" : [ -74.134288, 40.78977 ], "pop" : 13629, "state" : "NJ", "country" : "USA" }
{ "_id" : "07047", "city" : "NORTH BERGEN", "loc" : [ -74.017715, 40.793019 ], "pop" : 50823, "state" : "NJ", "country" : "USA" }
{ "_id" : "07060", "city" : "NORTH PLAINFIELD", "loc" : [ -74.425298, 40.61978 ], "pop" : 43471, "state" : "NJ", "country" : "USA" }
{ "_id" : "07063", "city" : "NORTH PLAINFIELD", "loc" : [ -74.445325, 40.604838 ], "pop" : 14285, "state" : "NJ", "country" : "USA" }
{ "_id" : "07062", "city" : "NORTH PLAINFIELD", "loc" : [ -74.406042, 40.631992 ], "pop" : 12756, "state" : "NJ", "country" : "USA" }
{ "_id" : "07102", "city" : "NEWARK", "loc" : [ -74.176505, 40.73201 ], "pop" : 10328, "state" : "NJ", "country" : "USA" }
{ "_id" : "07103", "city" : "NEWARK", "loc" : [ -74.196364, 40.736975 ], "pop" : 36949, "state" : "NJ", "country" : "USA" }
{ "_id" : "07105", "city" : "NEWARK", "loc" : [ -74.156346, 40.727086 ], "pop" : 38104, "state" : "NJ", "country" : "USA" }
{ "_id" : "07104", "city" : "NEWARK", "loc" : [ -74.1695, 40.766446 ], "pop" : 47183, "state" : "NJ", "country" : "USA" }
{ "_id" : "07107", "city" : "NEWARK", "loc" : [ -74.18816, 40.760656 ], "pop" : 36649, "state" : "NJ", "country" : "USA" }
{ "_id" : "07106", "city" : "NEWARK", "loc" : [ -74.233023, 40.741485 ], "pop" : 35013, "state" : "NJ", "country" : "USA" }
{ "_id" : "07108", "city" : "NEWARK", "loc" : [ -74.201538, 40.723647 ], "pop" : 29730, "state" : "NJ", "country" : "USA" }
{ "_id" : "07110", "city" : "NUTLEY", "loc" : [ -74.158934, 40.818548 ], "pop" : 26441, "state" : "NJ", "country" : "USA" }
{ "_id" : "07112", "city" : "NEWARK", "loc" : [ -74.213073, 40.71071 ], "pop" : 30356, "state" : "NJ", "country" : "USA" }
{ "_id" : "07114", "city" : "NEWARK", "loc" : [ -74.189105, 40.708246 ], "pop" : 11260, "state" : "NJ", "country" : "USA" }
{ "_id" : "07435", "city" : "NEWFOUNDLAND", "loc" : [ -74.435857, 41.064691 ], "pop" : 1624, "state" : "NJ", "country" : "USA" }
{ "_id" : "07646", "city" : "NEW MILFORD", "loc" : [ -74.019517, 40.933115 ], "pop" : 15799, "state" : "NJ", "country" : "USA" }
{ "_id" : "07648", "city" : "NORWOOD", "loc" : [ -73.95817, 40.995231 ], "pop" : 4858, "state" : "NJ", "country" : "USA" }
{ "_id" : "07748", "city" : "NEW MONMOUTH", "loc" : [ -74.113908, 40.396018 ], "pop" : 22727, "state" : "NJ", "country" : "USA" }
{ "_id" : "07753", "city" : "NEPTUNE CITY", "loc" : [ -74.054045, 40.211153 ], "pop" : 32147, "state" : "NJ", "country" : "USA" }
Type "it" for more
> db.zips.find({$and:[{$and:[{city:{$regex:"^N"}},{$or:[{state:"NY"},{state:"NJ"}]}]},{pop:{$gt:100000,$lt:1500000}}]}) ;
{ "_id" : "10021", "city" : "NEW YORK", "loc" : [ -73.958805, 40.768476 ], "pop" : 106564, "state" : "NY", "country" : "USA" }
{ "_id" : "10025", "city" : "NEW YORK", "loc" : [ -73.968312, 40.797466 ], "pop" : 100027, "state" : "NY", "country" : "USA" }
