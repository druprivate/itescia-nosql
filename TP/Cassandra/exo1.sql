/*
http://blog.ditullio.fr/2016/06/10/docker-docker-basics-cassandra/

curl -fsSL https://get.docker.com/ | sh
sudo usermod -aG docker drussier

fermer la session et rouvrir 

(docker container rm bb0615bf63c8393daacbe3ef783a6c184e59080d6e995cf51205773ba35a5ea0)
docker run -d --name=cass1 --net=host cassandra
(rm -rf ~/.cassandra)
docker exec -ti cass1 cqlsh localhost
*/

**************
-- TP


CREATE KEYSPACE IF NOT EXISTS ecole WITH REPLICATION = { 'class' : 'SimpleStrategy', 'replication_factor': 3 };
USE ecole;

CREATE TABLE Cours (
idCours INT, Intitule VARCHAR, Responsable INT, Niveau VARCHAR, nbHeuresMax INT, Coeff INT,
PRIMARY KEY ( idCours )
);
CREATE INDEX fk_Enseignement_Enseignant_idx ON Cours ( Responsable ) ;

CREATE TABLE Enseignant (
idEnseignant INT, Nom VARCHAR, Prenom VARCHAR, status VARCHAR,
PRIMARY KEY ( idEnseignant )
);
-- Pour vérifier si les tables ont bien été créées.
DESC ecole; 

INSERT INTO Cours (idCours,Intitule,Responsable,Niveau,nbHeuresMax,Coeff) VALUES (1,'Introduction aux Bases de Donnees',1,'M1',30,3);
INSERT INTO Cours (idCours,Intitule,Responsable,Niveau,nbHeuresMax,Coeff) VALUES (2,'Immeubles de Grandes Hauteurs',4,'M1',30,2);
INSERT INTO Cours (idCours,Intitule,Responsable,Niveau,nbHeuresMax,Coeff) VALUES (3,'Production et distribution de biens et de ser',5,'M1',30,2);
INSERT INTO Cours (idCours,Intitule,Responsable,Niveau,nbHeuresMax,Coeff) VALUES (4,'Bases de Donnees Avancees',1,'M2',30,5);
INSERT INTO Cours (idCours,Intitule,Responsable,Niveau,nbHeuresMax,Coeff) VALUES (5,'Architecture des Systemes Materiel',6,'M2',8,1);
INSERT INTO Cours (idCours,Intitule,Responsable,Niveau,nbHeuresMax,Coeff) VALUES (6,'IT Business / Introduction',7,'M2',20,3);
INSERT INTO Cours (idCours,Intitule,Responsable,Niveau,nbHeuresMax,Coeff) VALUES (7,'IT Business / Strategie et Management',8,'M2',10,1);

INSERT INTO Enseignant (idEnseignant,Nom,Prenom,status) VALUES (1,'Travers','Nicolas','Vacataire');
INSERT INTO Enseignant (idEnseignant,Nom,Prenom,status) VALUES (2,'Mourier','Pascale','Titulaire');
INSERT INTO Enseignant (idEnseignant,Nom,Prenom,status) VALUES (3,'Boisson','Francois','Vacataire');
INSERT INTO Enseignant (idEnseignant,Nom,Prenom,status) VALUES (4,'Mathieu','Eric','Titulaire');
INSERT INTO Enseignant (idEnseignant,Nom,Prenom,status) VALUES (5,'Chu','Chengbin','Titulaire');
INSERT INTO Enseignant (idEnseignant,Nom,Prenom,status) VALUES (6,'Boutin','Philippe','Titulaire');
INSERT INTO Enseignant (idEnseignant,Nom,Prenom,status) VALUES (7,'Escribe','Julien','Vacataire');
INSERT INTO Enseignant (idEnseignant,Nom,Prenom,status) VALUES (8,'Znaty','David','Vacataire');
INSERT INTO Enseignant (idEnseignant,Nom,Prenom,status) VALUES (9,'Abal-Kassim','Cheik Ahamed','Vacataire');

SELECT * FROM Cours;

/*
Pour interroger la base de données, la syntaxe CQL (pour Cassandra Query Language 1 ) est fortement inspirée
de SQL. Pour la suite des exercices, exprimer en CQL la requête demandée :
2.2.1 Liste tous les cours ;
*/
SELECT * FROM Cours;

-- 2.2.2 Liste des intitulés de cours ;
SELECT intitule FROM Cours;

-- 2.2.3 Nom de l’enseignant n°4 ;
SELECT nom FROM Enseignant WHERE idenseignant = 4;

-- 2.2.4 Intitulé des cours du responsable n° 1 ;
SELECT intitule FROM Cours WHERE responsable = 1;

-- 2.2.5 Intitulé des cours dont le nombre d’heures maximum est égal à 30 ;
CREATE INDEX cours_nbhrsmx_idx ON Cours ( nbHeuresMax ) ;
SELECT intitule FROM Cours WHERE nbheuresmax = 30;

-- 2.2.6 Intitulé des cours dont le responsable ’1’ et dont le niveau est ’M1’ ;
-- Utiliser ’ALLOW FILTERING’.
SELECT intitule
FROM Cours WHERE responsable = 1
AND niveau = 'M1'
ALLOW FILTERING ;

-- 2.2.7 Intitulé des cours dont les responsables ont une valeur inférieure à 5 ;
SELECT intitule
FROM Cours 
WHERE responsable <= 5
ALLOW FILTERING ;

-- 2.2.8 Intitulé des cours dont l’identifiant est inférieure à 5 ;
-- Utiliser la fonction ’token()’.

-- Ne fonctionne pas:
SELECT intitule FROM Cours  WHERE idcours <= 5;

SELECT intitule
FROM Cours 
WHERE token(idcours) <= 5;

SELECT intitule, idcours, token(idcours)
FROM Cours 
WHERE token(idcours) <= token(5);

-- 2.2.9 Compter le nombre de lignes retournées par la requête précédente ;
-- Utiliser ’COUNT(*)’
SELECT COUNT(*) FROM Cours ;

-- 9. La requête ci-dessous ne fonctionne pas, trouver une solution pour la faire fonctionner :
select nom, prenom from enseignant where status='Vacataire';
select nom, prenom from enseignant where status='Vacataire' allow filtering;

-- 10. Donner les intitulés des cours dont le statut du responsable est ’Vacataire’ ;
-- select idenseignant from enseignant where status='Vacataire' allow filtering;

-- On doit requêter une par une les réponses :
select intitule from cours where responsable = 1;
select intitule from cours where responsable = 8;
select intitule from cours where responsable = 7;
select intitule from cours where responsable = 9;
select intitule from cours where responsable = 3;

select intitule from cours where responsable in (1, 8, 7, 9, 3);
-- InvalidRequest: Error from server: code=2200 [Invalid query] message="IN predicates on non-primary-key columns (responsable) is not yet supported"

select intitule from cours where responsable in (select idenseignant from enseignant where status='Vacataire' allow filtering);
-- SyntaxException: line 1:49 mismatched input 'select' expecting ')' (... cours where responsable in ([select]...)

-- 11. La jointure n’est pas possible avec CQL (à cause du stockage par hachage distribué). Nous allons créer une solution alternative en fusionnant les deux tables. Pour cela, nous allons choisir d’intégrer la table ’Enseignant’ dans la table ’Cours’, nous appellerons cette table ’coursEnseignant’. Trois possibilités sont disponibles pour le faire : SET, LIST, MAP, SubType. Choisissez la solution qui permettra de faire un filtrage sur le statut de l’enseignant.

-- https://www.dbrnd.com/2016/05/nosql-cassandra-collection-data-types-list-set-map/
-- Avec une map (paire clé valeur) on pourra filtrer sur le statut de l'enseignant

-- 12.Créer la table et insérer les données suivantes :

CREATE TABLE CoursEnseignant (
idCours INT, Intitule VARCHAR, Responsable MAP<text,text>, Niveau VARCHAR, nbHeuresMax INT, Coeff INT,
PRIMARY KEY ( idCours )
);

INSERT INTO CoursEnseignant (idCours,Intitule,Responsable,Niveau,nbHeuresMax,Coeff) VALUES (1,'Introduction aux Bases de Donnees',{'idenseignant':'1','nom':'Travers','prenom':'Nicolas','status':'Vacataire'},'M1',30,3);
INSERT INTO CoursEnseignant (idCours,Intitule,Responsable,Niveau,nbHeuresMax,Coeff) VALUES (2,'Immeubles de Grandes Hauteurs',{'idenseignant':'4','nom':'Mathieu','prenom':'Eric','status':'Titulaire'},'M1',30,2);
INSERT INTO CoursEnseignant (idCours,Intitule,Responsable,Niveau,nbHeuresMax,Coeff) VALUES (3,'Production et distribution de biens et de ser',{'idenseignant':'5','nom':'Chu','prenom':'Chengbin','status':'Titulaire'},'M1',30,2);
INSERT INTO CoursEnseignant (idCours,Intitule,Responsable,Niveau,nbHeuresMax,Coeff) VALUES (4,'Bases de Donnees Avancees',{'idenseignant':'1','nom':'Travers','prenom':'Nicolas','status':'Vacataire'},'M2',30,5);
INSERT INTO CoursEnseignant (idCours,Intitule,Responsable,Niveau,nbHeuresMax,Coeff) VALUES (5,'Architecture des Systemes Materiel',{'idenseignant':'6','nom':'Boutin','prenom':'Philippe','status':'Titulaire'},'M2',8,1);
INSERT INTO CoursEnseignant (idCours,Intitule,Responsable,Niveau,nbHeuresMax,Coeff) VALUES (6,'IT Business / Introduction',{'idenseignant':'7','nom':'Escribe','prenom':'Julien','status':'Vacataire'},'M2',20,3);
INSERT INTO CoursEnseignant (idCours,Intitule,Responsable,Niveau,nbHeuresMax,Coeff) VALUES (7,'IT Business / Strategie et Management',{'idenseignant':'8','nom':'Znaty','prenom':'David','status':'Vacataire'},'M2',10,1);

-- 13. Créer un index sur le Niveau de la table CoursEnseignant
CREATE INDEX coursens_niveau_idx ON CoursEnseignant ( Niveau ) ;

-- 14. Donner les noms des responsables des cours (table CoursEnseignant) de niveau ’M1’
-- https://stackoverflow.com/questions/22016115/query-by-map-values-in-cassandra
select * from CoursEnseignant where Niveau = 'M1';
CREATE INDEX Responsable_idx ON CoursEnseignant (ENTRIES(Responsable));
SELECT * FROM CoursEnseignant WHERE Niveau = 'M1'; -- AND Responsable['nom'] LIKE '%';

-- 15. Donner l’intitulé des cours dont le responsable est vacataire
select intitule from CoursEnseignant where responsable contains 'Vacataire' allow filtering;


-- 16. Nous allons inverser la fusion des tables en créant une table ’EnseignantCours’ avec un sous-type ’Cours’. Pour pouvoir rajouter un ensemble de valeurs pour les cours d’un responsable, on peut utiliser : set, map ou list. Nous utiliserons le type map.
-- Créer le type ’Cours’ et la table ’EnseignantCours’

-- https://stackoverflow.com/questions/35036354/how-to-create-nested-collection-maptext-listtext-type-in-cassandra-database
-- https://stackoverflow.com/questions/31811502/inserting-multiple-types-in-map-in-cassandra

drop table EnseignantCours;

-- {idcours:4,intitule:'Bases de Donnees Avancees',niveau:'M2',nbHeuresMax:30,coeff:5}

CREATE TABLE EnseignantCours (
idEnseignant INT, Nom VARCHAR, Prenom VARCHAR, status VARCHAR, cours frozen<MAP<INT,MAP<text, text>>>,
PRIMARY KEY ( idEnseignant )
);

-- 17. Insérer les données suivantes :
INSERT INTO EnseignantCours (idEnseignant,Nom,Prenom,status,cours) VALUES (1,'Travers','Nicolas','Vacataire', {1:{'idcours':'1','intitule':'Introduction aux Bases de Donnees','niveau':'M1','nbHeuresMax':'30','Coeff':'3'}, 4:{'idcours':'4','intitule':'Bases de Donnees Avancees','niveau':'M2','nbHeuresMax':'30','coeff':'5'}});
INSERT INTO EnseignantCours (idEnseignant,Nom,Prenom,status,cours) VALUES (2,'Mourier','Pascale','Titulaire', {});
INSERT INTO EnseignantCours (idEnseignant,Nom,Prenom,status,cours) VALUES (3,'Boisson','Francois','Vacataire', {});
INSERT INTO EnseignantCours (idEnseignant,Nom,Prenom,status,cours) VALUES (4,'Mathieu','Eric','Titulaire', {4:{'idcours':'2','intitule':'Immeubles de Grandes Hauteurs','niveau':'M1','nbHeuresMax':'30','coeff':'2'}});
INSERT INTO EnseignantCours (idEnseignant,Nom,Prenom,status,cours) VALUES (5,'Chu','Chengbin','Titulaire', {});
INSERT INTO EnseignantCours (idEnseignant,Nom,Prenom,status,cours) VALUES (6,'Boutin','Philippe','Titulaire', {5:{'idcours':'5','intitule':'Architecture des Systemes Materiel','niveau':'M2','nbHeuresMax':'8','coeff':'1'}});
INSERT INTO EnseignantCours (idEnseignant,Nom,Prenom,status,cours) VALUES (7,'Escribe','Julien','Vacataire', {6:{'idcours':'6','intitule':'IT Business / Introduction','niveau':'M2','nbHeuresMax':'20','coeff':'3'}});
INSERT INTO EnseignantCours (idEnseignant,Nom,Prenom,status,cours) VALUES (8,'Znaty','David','Vacataire', {7:{'idcours':'7','intitule':'IT Business / Strategie et Management','niveau':'M2','nbHeuresMax':'10','coeff':'1'}});
INSERT INTO EnseignantCours (idEnseignant,Nom,Prenom,status,cours) VALUES (9,'Abal-Kassim','Cheik Ahamed','Vacataire', {});

-- 18. Créer un index sur le status de la table EnseignantCours
CREATE INDEX enscours_niveau_idx ON EnseignantCours ( Status ) ;

-- 19. Donner les intitulés des cours dont le responsable est Vacataire
select cours from enseignantcours where status = 'Vacataire';