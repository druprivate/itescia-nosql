mysql.server start
mysql -u root -p

http://sdz.tdct.org/sdz/administrez-vos-bases-de-donnees-avec-mysql.html
CREATE DATABASE elevage CHARACTER SET 'utf8';
USE db;


CREATE TABLE Animal (
    id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    espece VARCHAR(40) NOT NULL,
    sexe CHAR(1),
    date_naissance DATETIME NOT NULL,
    nom VARCHAR(30),
    commentaires TEXT,
    PRIMARY KEY (id)
)
ENGINE=INNODB;

-- annuaire
DROP TABLE if EXISTS Annuaire;
CREATE TABLE Annuaire (
    id      SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    nom     VARCHAR(40) NOT NULL,
    prenom  VARCHAR(40) NOT NULL,
    code_postal    integer NOT NULL,
    ville   VARCHAR(40) NOT NULL,
    adresse VARCHAR(100) NOT NULL,
    tel     CHAR(10) NOT NULL,
    PRIMARY KEY (id)
);

INSERT INTO Annuaire VALUES
(NULL,'DUPONT','Pierre',75012,'PARIS','1, place de la Nation','0153078990'),
(NULL,'MERCIER','Laure',69005,'LYON','10, rue des Noyers','0472578353'),
(NULL,'QUESSADA','Laurent',13001,'MARSEILLE','33, avenue du Prado','0453940263')
-- (NULL,'','',,'','','')
;
SELECT * from annuaire;


-- CV


-- facebook
DROP TABLE if EXISTS personnes;
CREATE TABLE personnes (
    id      BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    nom     VARCHAR(40) NOT NULL,
    prenom  VARCHAR(40) NOT NULL,
    code_postal    integer NOT NULL,
    sexe CHAR(1),
    date_naissance DATETIME NOT NULL,
    PRIMARY KEY (id)
);
INSERT INTO personnes VALUES
(NULL,'DUPONT','Pierre',75012,'M',cast('1996-02-12' as datetime)),
(NULL,'MERCIER','Laure',69005,'F',cast('1980-10-23' as datetime)),
(NULL,'QUESSADA','Laurent',13001,'M',cast('2001-12-05' as datetime))
-- (NULL,'','',,'','','')
;
SELECT * from personnes;


DROP TABLE if EXISTS amis;
CREATE TABLE Annuaire (
    id          BIGINT NOT NULL,
    id_ami      BIGINT NOT NULL,
    PRIMARY KEY (id)
);
INSERT INTO amis VALUES
(1,2),
(1,3)
;
SELECT * from amis;


-- mes amis
SELECT *
FROM personnes AS a
INNER JOIN (
SELECT id_ami
FROM amis
WHERE id = (SELECT id FROM personnes WHERE nom = 'RUSSIER' AND prenom = 'Damien' AND date_naissance = '27/05/1985')
) AS b ON a.id = b.id_ami
;

mysqld stop