# -*- coding: utf-8 -*-
import pdfkit

if __name__ == '__main__':
    BASEURL = "https://openclassrooms.com/courses/maitrisez-les-bases-de-donnees-nosql"
    urls = [
        "",
        "/orientez-vous-vers-le-cle-valeur",
        "/qu-est-ce-qu-un-oriente-colonnes",
        "/pourquoi-pas-le-oriente-documents",
        "/et-les-graphes-dans-tout-ca",
        "/exercises/1686",
        "/decouvrez-le-fonctionnement-de-mongodb",
        "/apprenez-un-nouveau-langage-pour-interroger-vos-donnees",
        "/distribuez-vos-donnees-avec-mongodb",
        "/protegez-vous-des-pannes-avec-les-replicaset",
        "/exercises/1731",
        "/etudiez-le-fonctionnement-d-elasticsearch",
        "/interrogez-des-donnees-textuelles",
        "/faites-grandir-votre-baseB",
        "/visualisez-et-prototypez-avec-kibana",
        "/exercises/1919"
    ]
    for ii, url, in enumerate(urls):
        uu = BASEURL + url
        print("*** ", uu)
        pdfkit.from_url(uu, "NoSQL{:02d}.pdf".format(ii))
